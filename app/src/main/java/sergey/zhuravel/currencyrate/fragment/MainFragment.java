package sergey.zhuravel.currencyrate.fragment;

import android.app.Fragment;
import android.app.ProgressDialog;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.google.gson.Gson;
import com.google.gson.JsonElement;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

import io.realm.Realm;
import io.realm.RealmResults;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import sergey.zhuravel.currencyrate.JsonApi;
import sergey.zhuravel.currencyrate.R;
import sergey.zhuravel.currencyrate.adapter.DataAdapter;
import sergey.zhuravel.currencyrate.model.Course;
import sergey.zhuravel.currencyrate.model.ValuteBTC;
import sergey.zhuravel.currencyrate.model.ValuteEUR;
import sergey.zhuravel.currencyrate.model.ValuteRUB;
import sergey.zhuravel.currencyrate.model.ValuteUSD;
import sergey.zhuravel.currencyrate.utils.RealmController;


public class MainFragment extends Fragment {

    public static final String BASE_URL = "https://api.privatbank.ua";
    private ArrayList<Course> courseArrayList = new ArrayList<>();
    private Realm realm;
    private SwipeRefreshLayout swipe;
    private RecyclerView recyclerView;
    private DataAdapter adapter;


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.setRetainInstance(true);
        this.realm = RealmController.with(getActivity()).getRealm();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.main_fragment, container, false);

        recyclerView = (RecyclerView) view.findViewById(R.id.card_recycler_view);

        recyclerView.setHasFixedSize(true);
        GridLayoutManager gridLayoutManager = new GridLayoutManager(getActivity(), 2);
        recyclerView.setLayoutManager(gridLayoutManager);


        adapter = new DataAdapter(courseArrayList, getActivity());
        recyclerView.setAdapter(adapter);


        swipe = (SwipeRefreshLayout) view.findViewById(R.id.swipe);
        swipe.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                loadJson();
                swipe.setRefreshing(false);
            }
        });

        setStartValute();
//        ((AppCompatActivity) getActivity()).getSupportActionBar().setSubtitle("");


        return view;
    }


    private void loadJson() {
        ProgressDialog pd = ProgressDialog.show(getActivity(), "Курс валют", "Загрузка данных ...", false, false);

        Retrofit retrofit = new Retrofit.Builder().baseUrl(BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();
        JsonApi registerAPI = retrofit.create(JsonApi.class);

        registerAPI.getCourses().enqueue(new Callback<JsonElement>() {
            @Override
            public void onResponse(Call<JsonElement> call, Response<JsonElement> response) {
                String myResponse = response.body().toString();
                Gson gson = new Gson();
                Course[] courses;
                courses = gson.fromJson(myResponse, Course[].class);
                for (final Course course : courses) {
                    courseArrayList.add(course);
                }

                for (Course course : courseArrayList) {
                    switch (course.getCcy()) {
                        case "USD":
                            isContainsUSD(course);
                            break;
                        case "EUR":
                            isContainsEUR(course);
                            break;
                        case "RUB":
                        case "RUR":
                            isContainsRUB(course);
                            break;
                        case "BTC":
                            isContainsBTC(course);
                            break;
                    }
                }

                adapter = new DataAdapter(courseArrayList, getActivity());
                recyclerView.setAdapter(adapter);
            }

            @Override
            public void onFailure(Call<JsonElement> call, Throwable t) {
                Snackbar.make(getView(), R.string.error_connect, Snackbar.LENGTH_SHORT).show();
            }
        });

        pd.dismiss();
        courseArrayList.clear();
    }


    private void isContainsEUR(Course course) {

        String buy = null, sale = null, date = null;

        RealmResults<ValuteEUR> eurRealmResults = realm.where(ValuteEUR.class).findAll();
        if (eurRealmResults.size() != 0) {
            for (ValuteEUR eur : eurRealmResults) {
                date = eur.getDate();
                buy = eur.getBuy();
                sale = eur.getSale();
            }

            if (formatStringToDate(date).equals(formatDate(new Date()))) {
                if (course.getBuy().equals(buy) || course.getSale().equals(sale)) {
                    Snackbar.make(getView(), R.string.new_data_isnt, Snackbar.LENGTH_SHORT).show();
                } else {
                    Snackbar.make(getView(), R.string.today_add_new_data_eur, Snackbar.LENGTH_SHORT).show();
                    addEURToDB(course);
                }
            } else {
                Snackbar.make(getView(), R.string.data_successfully_added, Snackbar.LENGTH_SHORT).show();
                addEURToDB(course);
            }

        } else {
            addEURToDB(course);
        }
    }

    private void isContainsUSD(Course course) {

        String buy = null, sale = null, date = null;

        RealmResults<ValuteUSD> usdRealmResults = realm.where(ValuteUSD.class).findAll();
        if (usdRealmResults.size() != 0) {
            for (ValuteUSD usd : usdRealmResults) {
                date = usd.getDate();
                buy = usd.getBuy();
                sale = usd.getSale();
            }

            if (formatStringToDate(date).equals(formatDate(new Date()))) {
                if (course.getBuy().equals(buy) || course.getSale().equals(sale)) {
                    Snackbar.make(getView(), R.string.new_data_isnt, Snackbar.LENGTH_SHORT).show();
                } else {
                    Snackbar.make(getView(), R.string.today_add_new_data_usd, Snackbar.LENGTH_SHORT).show();
                    addUSDToDB(course);
                }
            } else {
                Snackbar.make(getView(), R.string.data_successfully_added, Snackbar.LENGTH_SHORT).show();
                addUSDToDB(course);
            }

        } else {
            addUSDToDB(course);
        }
    }

    private void isContainsRUB(Course course) {

        String buy = null, sale = null, date = null;

        RealmResults<ValuteRUB> rubRealmResults = realm.where(ValuteRUB.class).findAll();
        if (rubRealmResults.size() != 0) {
            for (ValuteRUB rub : rubRealmResults) {
                date = rub.getDate();
                buy = rub.getBuy();
                sale = rub.getSale();
            }

            if (formatStringToDate(date).equals(formatDate(new Date()))) {
                if (course.getBuy().equals(buy) || course.getSale().equals(sale)) {
                    Snackbar.make(getView(), R.string.new_data_isnt, Snackbar.LENGTH_SHORT).show();
                } else {
                    Snackbar.make(getView(), R.string.today_add_new_data_rub, Snackbar.LENGTH_SHORT).show();
                    addRUBToDB(course);
                }
            } else {
                Snackbar.make(getView(), R.string.data_successfully_added, Snackbar.LENGTH_SHORT).show();
                addRUBToDB(course);
            }

        } else {
            addRUBToDB(course);
        }
    }

    private void isContainsBTC(Course course) {

        String buy = null, sale = null, date = null;

        RealmResults<ValuteBTC> btcRealmResults = realm.where(ValuteBTC.class).findAll();
        if (btcRealmResults.size() != 0) {
            for (ValuteBTC btc : btcRealmResults) {
                date = btc.getDate();
                buy = btc.getBuy();
                sale = btc.getSale();
            }

            if (formatStringToDate(date).equals(formatDate(new Date()))) {
                if (course.getBuy().equals(buy) || course.getSale().equals(sale)) {
                    Snackbar.make(getView(), R.string.new_data_isnt, Snackbar.LENGTH_SHORT).show();
                } else {
                    Snackbar.make(getView(), R.string.today_add_new_data_btc, Snackbar.LENGTH_SHORT).show();
                    addBTCToDB(course);
                }
            } else {
                Snackbar.make(getView(), R.string.data_successfully_added, Snackbar.LENGTH_SHORT).show();
                addBTCToDB(course);
            }

        } else {
            addBTCToDB(course);
        }
    }



    private void setStartValute() {
        getLastValue();

        adapter = new DataAdapter(courseArrayList, getActivity());
        recyclerView.setAdapter(adapter);


    }

    private void getLastValue() {
        courseArrayList.clear();
        getActivity().runOnUiThread(new Runnable() {
            @Override
            public void run() {
//                usd
                RealmResults<ValuteUSD> usds = realm.where(ValuteUSD.class).findAll();
                if (usds.size() != 0) {
                    String buyUSD = null, saleUSD = null, date = null;
                    for (ValuteUSD usd : usds) {
                        buyUSD = usd.getBuy();
                        saleUSD = usd.getSale();
                        date = usd.getDate();
                    }
                    Course course = new Course("USD", "UAH", buyUSD, saleUSD, date);
                    courseArrayList.add(course);

                }
//                eur
                RealmResults<ValuteEUR> eurs = realm.where(ValuteEUR.class).findAll();
                if (eurs.size() != 0) {
                    String buyEUR = null, saleEUR = null, date = null;

                    for (ValuteEUR eur : eurs) {
                        buyEUR = eur.getBuy();
                        saleEUR = eur.getSale();
                        date = eur.getDate();

                    }
                    Course course = new Course("EUR", "UAH", buyEUR, saleEUR, date);
                    courseArrayList.add(course);
                }
//                rub
                RealmResults<ValuteRUB> rubs = realm.where(ValuteRUB.class).findAll();
                if (rubs.size() != 0) {
                    String buyRUB = null, saleRUB = null, date = null;

                    for (ValuteRUB rub : rubs) {
                        buyRUB = rub.getBuy();
                        saleRUB = rub.getSale();
                        date = rub.getDate();

                    }
                    Course course = new Course("RUB", "UAH", buyRUB, saleRUB, date);
                    courseArrayList.add(course);
                }
//                btc
                RealmResults<ValuteBTC> btcs = realm.where(ValuteBTC.class).findAll();
                if (btcs.size() != 0) {
                    String buyBTC = null, saleBTC = null, date = null;

                    for (ValuteBTC btc : btcs) {
                        buyBTC = btc.getBuy();
                        saleBTC = btc.getSale();
                        date = btc.getDate();

                    }
                    Course course = new Course("BTC", "USD", buyBTC, saleBTC, date);
                    courseArrayList.add(course);
                    ((AppCompatActivity) getActivity()).getSupportActionBar().setSubtitle(getString(R.string.date_last_update) + formatStringToDateFull(date));

                }

            }
        });
    }

    private void addUSDToDB(Course course) {
        realm.beginTransaction();
        ValuteUSD valuteUSD = new ValuteUSD();
        valuteUSD.setBuy(course.getBuy());
        valuteUSD.setSale(course.getSale());
        valuteUSD.setDate(String.valueOf(new Date()));
        valuteUSD.setId(RealmController.getInstance().getUSD().size() + System.currentTimeMillis());
        realm.copyToRealm(valuteUSD);
        realm.commitTransaction();
    }

    private void addEURToDB(Course course) {
        realm.beginTransaction();
        ValuteEUR valuteEUR = new ValuteEUR();
        valuteEUR.setBuy(course.getBuy());
        valuteEUR.setSale(course.getSale());
        valuteEUR.setDate(String.valueOf(new Date(System.currentTimeMillis())));
        valuteEUR.setId(RealmController.getInstance().getEUR().size() + System.currentTimeMillis());
        realm.copyToRealm(valuteEUR);
        realm.commitTransaction();
    }

    private void addRUBToDB(Course course) {
        realm.beginTransaction();
        ValuteRUB valuteRUB = new ValuteRUB();
        valuteRUB.setBuy(course.getBuy());
        valuteRUB.setSale(course.getSale());
        valuteRUB.setDate(String.valueOf(new Date(System.currentTimeMillis())));
        valuteRUB.setId(RealmController.getInstance().getRUB().size() + System.currentTimeMillis());
        realm.copyToRealm(valuteRUB);
        realm.commitTransaction();
    }

    private void addBTCToDB(Course course) {
        realm.beginTransaction();
        ValuteBTC valuteBTC = new ValuteBTC();
        valuteBTC.setBuy(course.getBuy());
        valuteBTC.setSale(course.getSale());
        valuteBTC.setDate(String.valueOf(new Date(System.currentTimeMillis())));
        valuteBTC.setId(RealmController.getInstance().getBTC().size() + System.currentTimeMillis());
        realm.copyToRealm(valuteBTC);
        realm.commitTransaction();
    }



    public static String formatStringToDate(String date) {

        DateFormat df = new SimpleDateFormat("EEE MMM dd kk:mm:ss Z yyyy");
        Date d;
        try {
            d = df.parse(date);
        } catch (ParseException e) {
            d = new Date();
            e.printStackTrace();
        }
        return formatDate(d);
    }

    public static String formatDate(Date date) {
        SimpleDateFormat dateFormat = new SimpleDateFormat("dd.MM.yyyy");
        return dateFormat.format(date);
    }


    public static String formatStringToDateFull(String date) {
        DateFormat df = new SimpleDateFormat("EEE MMM dd kk:mm:ss Z yyyy");
        Date d;
        try {
            d = df.parse(date);
        } catch (ParseException e) {
            d = new Date();
            e.printStackTrace();
        }
        return new SimpleDateFormat("dd.MM.yyyy | kk:mm").format(d);


    }

    public static String formatStringToDateShort(String date) {
        DateFormat df = new SimpleDateFormat("EEE MMM dd kk:mm:ss Z yyyy");
        Date d;
        try {
            d = df.parse(date);
        } catch (ParseException e) {
            d = new Date();
            e.printStackTrace();
        }
        return new SimpleDateFormat("dd.MM").format(d);
    }
    public static String formatStringToDateShortTime(String date) {
        DateFormat df = new SimpleDateFormat("EEE MMM dd kk:mm:ss Z yyyy");
        Date d;
        try {
            d = df.parse(date);
        } catch (ParseException e) {
            d = new Date();
            e.printStackTrace();
        }
        return new SimpleDateFormat("dd.MM|kk:mm").format(d);
    }

}
