package sergey.zhuravel.currencyrate.fragment;


import android.app.Fragment;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.github.mikephil.charting.charts.LineChart;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.data.LineData;
import com.github.mikephil.charting.data.LineDataSet;

import java.util.ArrayList;
import java.util.List;

import co.moonmonkeylabs.realmrecyclerview.RealmRecyclerView;
import io.realm.Realm;
import io.realm.RealmResults;
import sergey.zhuravel.currencyrate.R;
import sergey.zhuravel.currencyrate.adapter.ValuteBTCAdapter;
import sergey.zhuravel.currencyrate.adapter.ValuteEURAdapter;
import sergey.zhuravel.currencyrate.adapter.ValuteRUBAdapter;
import sergey.zhuravel.currencyrate.adapter.ValuteUSDAdapter;
import sergey.zhuravel.currencyrate.model.ValuteBTC;
import sergey.zhuravel.currencyrate.model.ValuteEUR;
import sergey.zhuravel.currencyrate.model.ValuteRUB;
import sergey.zhuravel.currencyrate.model.ValuteUSD;
import sergey.zhuravel.currencyrate.utils.RealmController;


public class DetailsFragment extends Fragment {


    private LineChart chart;
    private String text;
    private Realm realm;
    private RealmRecyclerView realmRecyclerView;
    private ArrayList<Entry> saleList = new ArrayList<>();
    private ArrayList<Entry> buyList = new ArrayList<>();
    private ArrayList<String> arrayDate = new ArrayList<>();


    public static DetailsFragment newInstance(String ccy) {
        Bundle args = new Bundle();
        args.putString("ccy", ccy);
        DetailsFragment fragment = new DetailsFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        this.setRetainInstance(true);
        this.realm = RealmController.with(getActivity()).getRealm();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.details_fragment, container, false);
        text = getArguments().getString("ccy");

        chart = (LineChart) view.findViewById(R.id.chart1);

        realmRecyclerView = (RealmRecyclerView) view.findViewById(R.id.realm_recycler_view);


        switch (text) {
            case "USD":
                RealmResults<ValuteUSD> usdRealmResults = realm.where(ValuteUSD.class).findAllSorted("date", false);
                ValuteUSDAdapter valuteUSDAdapter = new ValuteUSDAdapter(getActivity(), usdRealmResults, true, true);
                realmRecyclerView.setAdapter(valuteUSDAdapter);
                ((AppCompatActivity) getActivity()).getSupportActionBar().setSubtitle(getString(R.string.history_usd));

                for (int i = 0; i < usdRealmResults.size(); i++) {
                    arrayDate.add(MainFragment.formatStringToDateShort(usdRealmResults.get(i).getDate()));
                    saleList.add(new Entry(Float.parseFloat(usdRealmResults.get(i).getSale()), i));
                    buyList.add(new Entry(Float.parseFloat(usdRealmResults.get(i).getBuy()), i));
                }

                drawCharts(saleList, buyList, arrayDate);
                break;
            case "EUR":
                RealmResults<ValuteEUR> eurRealmResults = realm.where(ValuteEUR.class).findAllSorted("date", false);
                ValuteEURAdapter valuteEURAdapter = new ValuteEURAdapter(getActivity(), eurRealmResults, true, true);
                realmRecyclerView.setAdapter(valuteEURAdapter);
                ((AppCompatActivity) getActivity()).getSupportActionBar().setSubtitle(getString(R.string.history_eur));

                for (int i = 0; i < eurRealmResults.size(); i++) {
                    arrayDate.add(MainFragment.formatStringToDateShort(eurRealmResults.get(i).getDate()));
                    saleList.add(new Entry(Float.parseFloat(eurRealmResults.get(i).getSale()), i));
                    buyList.add(new Entry(Float.parseFloat(eurRealmResults.get(i).getBuy()), i));
                }

                drawCharts(saleList, buyList, arrayDate);

                break;
            case "RUB":
            case "RUR":
                RealmResults<ValuteRUB> rubRealmResults = realm.where(ValuteRUB.class).findAllSorted("date", false);
                ValuteRUBAdapter valuteRUBAdapter = new ValuteRUBAdapter(getActivity(), rubRealmResults, true, true);
                realmRecyclerView.setAdapter(valuteRUBAdapter);
                ((AppCompatActivity) getActivity()).getSupportActionBar().setSubtitle(getString(R.string.history_rub));

                for (int i = 0; i < rubRealmResults.size(); i++) {
                    arrayDate.add(MainFragment.formatStringToDateShort(rubRealmResults.get(i).getDate()));
                    saleList.add(new Entry(Float.parseFloat(rubRealmResults.get(i).getSale()), i));
                    buyList.add(new Entry(Float.parseFloat(rubRealmResults.get(i).getBuy()), i));
                }

                drawCharts(saleList, buyList, arrayDate);

                break;
            case "BTC":
                RealmResults<ValuteBTC> btcRealmResults = realm.where(ValuteBTC.class).findAllSorted("date", false);
                ValuteBTCAdapter valuteBTCAdapter = new ValuteBTCAdapter(getActivity(), btcRealmResults, true, true);
                realmRecyclerView.setAdapter(valuteBTCAdapter);
                ((AppCompatActivity) getActivity()).getSupportActionBar().setSubtitle(getString(R.string.history_btc));


                for (int i = 0; i < btcRealmResults.size(); i++) {
                    arrayDate.add(MainFragment.formatStringToDateShortTime(btcRealmResults.get(i).getDate()));
                    saleList.add(new Entry(Float.parseFloat(btcRealmResults.get(i).getSale()), i));
                    buyList.add(new Entry(Float.parseFloat(btcRealmResults.get(i).getBuy()), i));
                }

                drawCharts(saleList, buyList, arrayDate);


                break;

        }


        return view;

    }


    private void drawCharts(ArrayList<Entry> saleList, ArrayList<Entry> buyList, ArrayList<String> arrayDate) {
        LineDataSet dataSetSale = new LineDataSet(saleList, "Sale");
        LineDataSet dataSetBuy = new LineDataSet(buyList, "Buy");

        List<LineDataSet> lineDataSets = new ArrayList<>();
        lineDataSets.add(dataSetSale);
        lineDataSets.add(dataSetBuy);


        LineData data = new LineData(arrayDate, lineDataSets);

        dataSetSale.setColors(new int[]{Color.RED});
        dataSetSale.setDrawCubic(true);

        dataSetBuy.setDrawCubic(true);

        chart.setData(data);
        chart.animateY(3000);
    }

}
