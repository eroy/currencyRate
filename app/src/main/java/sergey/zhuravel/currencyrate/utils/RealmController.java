package sergey.zhuravel.currencyrate.utils;

import android.app.Activity;
import android.app.Application;
import android.app.Fragment;

import io.realm.Realm;
import io.realm.RealmResults;
import sergey.zhuravel.currencyrate.model.ValuteBTC;
import sergey.zhuravel.currencyrate.model.ValuteEUR;
import sergey.zhuravel.currencyrate.model.ValuteRUB;
import sergey.zhuravel.currencyrate.model.ValuteUSD;


public class RealmController {

    private static RealmController instance;
    private final Realm realm;
    public RealmController(Application application) {
        realm = Realm.getDefaultInstance();
    }
    public static RealmController with(Fragment fragment) {
        if (instance == null) {
            instance = new RealmController(fragment.getActivity().getApplication());
        }
        return instance;
    }
    public static RealmController with(Activity activity) {
        if (instance == null) {
            instance = new RealmController(activity.getApplication());
        }
        return instance;
    }
    public static RealmController with(Application application) {
        if (instance == null) {
            instance = new RealmController(application);
        }
        return instance;
    }
    public static RealmController getInstance() {
        return instance;
    }
    public Realm getRealm() {
        return realm;
    }


    //find all objects in the Communal
    public RealmResults<ValuteUSD> getUSD() {
        return realm.where(ValuteUSD.class).findAll();
    }

    public RealmResults<ValuteEUR> getEUR() {
        return realm.where(ValuteEUR.class).findAll();
    }

    public RealmResults<ValuteRUB> getRUB() {
        return realm.where(ValuteRUB.class).findAll();
    }

    public RealmResults<ValuteBTC> getBTC() {
        return realm.where(ValuteBTC.class).findAll();
    }

}
