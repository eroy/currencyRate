package sergey.zhuravel.currencyrate.model;


import java.io.Serializable;

public class Course {
    private String ccy;
    private String base_ccy;
    private String buy;
    private String sale;

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    private String date;

    public Course(String ccy, String base_ccy, String buy, String sale, String date) {
        this.ccy = ccy;
        this.base_ccy = base_ccy;
        this.buy = buy;
        this.sale = sale;
        this.date = date;
    }

    public String getCcy() {
        return ccy;
    }

    public void setCcy(String ccy) {
        this.ccy = ccy;
    }

    public String getBase_ccy() {
        return base_ccy;
    }

    public void setBase_ccy(String base_ccy) {
        this.base_ccy = base_ccy;
    }

    public String getBuy() {
        return buy;
    }

    public void setBuy(String buy) {
        this.buy = buy;
    }

    public String getSale() {
        return sale;
    }

    public void setSale(String sale) {
        this.sale = sale;
    }

    @Override
    public String toString() {
        return getCcy() + " to " + getBase_ccy() + " [" +
                "\nbuy=" + buy +
                ", \nsale=" + sale +
                "]\n";
    }
}
