package sergey.zhuravel.currencyrate.model;


import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

public class ValuteEUR extends RealmObject {

    @PrimaryKey
    private long id;
    private String buy;
    private String sale;
    private String date;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getBuy() {
        return buy;
    }

    public void setBuy(String buy) {
        this.buy = buy;
    }

    public String getSale() {
        return sale;
    }

    public void setSale(String sale) {
        this.sale = sale;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }
}
