package sergey.zhuravel.currencyrate.alarm;

import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.IBinder;
import android.support.v4.app.NotificationCompat;
import android.telephony.SmsManager;
import android.util.Log;

import com.google.gson.Gson;
import com.google.gson.stream.JsonReader;

import java.io.IOException;
import java.io.InputStreamReader;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.Date;

import javax.net.ssl.HttpsURLConnection;

import sergey.zhuravel.currencyrate.MainActivity;
import sergey.zhuravel.currencyrate.R;
import sergey.zhuravel.currencyrate.fragment.MainFragment;
import sergey.zhuravel.currencyrate.model.Course;

public class MyService extends Service {
    public String text = "123";

    public MyService() {
    }

    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        Log.d("Sergey", "onstartcommand");
        syncDataCourse();
        try {
            Thread.sleep(5000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        setNotification(text);


        asyncSendSms(text, "0730385520");
        return START_STICKY;
    }

    private void setNotification(String text) {
        Context context = this.getApplicationContext();

        NotificationManager notificationManager = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);

        Intent reating_intent = new Intent(context, MainActivity.class);
        reating_intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);

        PendingIntent pendingIntent = PendingIntent.getActivity(context, 100, reating_intent, PendingIntent.FLAG_UPDATE_CURRENT);


        NotificationCompat.Builder builder = new NotificationCompat.Builder(context)
                .setContentIntent(pendingIntent)
                .setSmallIcon(R.mipmap.ic_launcher)
                .setContentTitle("Myalarm titile")
                .setContentText(text)

                .setAutoCancel(true);


        notificationManager.notify(100, builder.build());
    }

    private void syncDataCourse() {
        Thread thread = new Thread(new Runnable() {
            @Override
            public void run() {
                Course[] arrayCourse;
                try {
//                    url, json
                    URL url = new URL("https://api.privatbank.ua/p24api/pubinfo?exchange&json&coursid=11");
                    Log.d("Sergey", "uri ok");
//                    if url -https - used HttpsURLConnection, else HttpURLConnection
                    HttpsURLConnection connection = (HttpsURLConnection) url.openConnection();
                    Gson gson = new Gson();
                    JsonReader reader = new JsonReader(new InputStreamReader(connection.getInputStream()));
                    arrayCourse = gson.fromJson(reader, Course[].class);

                    for (final Course course : arrayCourse) {
//                        add to arraylist only USD or EUR
                        if (course.getCcy().equals("USD")) {
                            StringBuilder stringBuilder = new StringBuilder();
                            stringBuilder.append("This sms auto generate.\n");
                            stringBuilder.append("Today " + MainFragment.formatDate(new Date()) + "\n");
                            stringBuilder.append(course.getCcy() + " to " + course.getBase_ccy() + "\n");
                            stringBuilder.append("Buy: " + course.getBuy() + "\n");
                            stringBuilder.append("Sale: " + course.getSale() + "\n");
                            stringBuilder.append("Good luck!");
                            text = stringBuilder.toString();

                            Log.d("Sergey", "text ok - " + text);

                        }
                    }

                } catch (MalformedURLException e) {
                    Log.d("Sergey", e.toString());
                    Log.d("Sergey", "ERROR");
                    e.printStackTrace();

                } catch (IOException e) {
                    Log.d("Sergey", e.toString());
                    Log.d("Sergey", "ERROR ALL");

                    e.printStackTrace();
                }

                Log.d("Sergey", "sync ok");

            }
        });
        thread.start();


    }


    public void asyncSendSms(final String text, final String phone) {

        AsyncTask<Void, Void, Void> remoteItem = new AsyncTask<Void, Void, Void>() {


            @Override
            protected Void doInBackground(Void... params) {
                try {
                    SmsManager smsManager = SmsManager.getDefault();
                    smsManager.sendTextMessage(phone, null, text, null, null);
                } catch (Exception e) {

                    e.printStackTrace();
                }
                return null;
            }

            @Override
            protected void onPostExecute(Void aVoid) {
                Log.d("Sergey", "sms send:\nwith text: " + text + "\non number: " + phone);

            }
        };
        remoteItem.execute();
    }
}
