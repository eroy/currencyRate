package sergey.zhuravel.currencyrate.adapter;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import io.realm.Realm;
import io.realm.RealmBasedRecyclerViewAdapter;
import io.realm.RealmResults;
import sergey.zhuravel.currencyrate.R;
import sergey.zhuravel.currencyrate.fragment.MainFragment;
import sergey.zhuravel.currencyrate.model.ValuteRUB;
import sergey.zhuravel.currencyrate.utils.RealmController;


public class ValuteRUBAdapter extends RealmBasedRecyclerViewAdapter<ValuteRUB, ViewHolderValuteRVH> {
    private Realm realm;
    private Context context;

    public ValuteRUBAdapter(Context context, RealmResults<ValuteRUB> realmResults, boolean automaticUpdate, boolean animateResults) {
        super(context, realmResults, automaticUpdate, animateResults);
        this.context = context;
    }

    @Override
    public ViewHolderValuteRVH onCreateRealmViewHolder(ViewGroup viewGroup, int i) {
        View view = inflater.inflate(R.layout.card_details, viewGroup, false);
        return new ViewHolderValuteRVH((LinearLayout) view);
    }

    @Override
    public void onBindRealmViewHolder(ViewHolderValuteRVH viewHolderRecyclerView, int i) {
        realm = RealmController.getInstance().getRealm();

        final ValuteRUB valuteRUB = realmResults.get(i);

        viewHolderRecyclerView.date.setText(MainFragment.formatStringToDate(valuteRUB.getDate()));
        viewHolderRecyclerView.buy.setText(valuteRUB.getBuy());
        viewHolderRecyclerView.sale.setText(valuteRUB.getSale());


        viewHolderRecyclerView.field.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
//                etc..
            }
        });


    }


    @Override
    public int getItemCount() {
        return realmResults.size();
    }

}