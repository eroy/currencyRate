package sergey.zhuravel.currencyrate.adapter;


import android.content.Context;
import android.content.DialogInterface;
import android.os.AsyncTask;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.RecyclerView;
import android.telephony.SmsManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.Date;

import io.realm.Realm;
import sergey.zhuravel.currencyrate.R;
import sergey.zhuravel.currencyrate.fragment.DetailsFragment;
import sergey.zhuravel.currencyrate.fragment.MainFragment;
import sergey.zhuravel.currencyrate.model.Course;
import sergey.zhuravel.currencyrate.utils.RealmController;

public class DataAdapter extends RecyclerView.Adapter<DataAdapter.ViewHolder> {
    private ArrayList<Course> courses;
    private Context context;
    private Realm realm;


    public DataAdapter(ArrayList<Course> courses, Context context) {
        this.courses = courses;
        this.context = context;

    }

    @Override
    public DataAdapter.ViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.card_row, viewGroup, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(DataAdapter.ViewHolder viewHolder, final int i) {
        realm = RealmController.getInstance().getRealm();
        final Course course = courses.get(i);

        viewHolder.ccy.setText(course.getCcy());
        viewHolder.baseCcy.setText(course.getBase_ccy());
        viewHolder.buy.setText(formatNumber(course.getBuy()));
        viewHolder.sale.setText(formatNumber(course.getSale()));
        viewHolder.img.setImageResource(getIcon(course.getCcy()));

        viewHolder.field.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View v) {

                showAlert(getString(course));

                return true;
            }
        });

        viewHolder.field.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                ((AppCompatActivity) context).getFragmentManager().beginTransaction()
                        .replace(R.id.frame_layout, DetailsFragment.newInstance(course.getCcy()))
                        .addToBackStack(null).commit();

            }
        });

    }

//    format number, 2 characters after the decimal point
    private String formatNumber(String text) {
        return String.format("%.2f", Float.parseFloat(text));
    }

    private String getString(Course course) {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("Hello!!\n");
        stringBuilder.append("Today " + MainFragment.formatDate(new Date()) + "\n");
        stringBuilder.append(course.getCcy() + " to " + course.getBase_ccy() + "\n");
        stringBuilder.append("Buy: " + course.getBuy() + "\n");
        stringBuilder.append("Sale: " + course.getSale() + "\n");
        stringBuilder.append("Good luck!");
        return stringBuilder.toString();
    }


    public void showAlert(final String text) {

        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setTitle("Отправка смс");
        builder.setMessage("Отправить смс на номер 0664809526(Елена) c текстом:\n\"" + text + "\"")
                .setCancelable(false)
                .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        asyncSendSms(text, "0664809526");
                    }
                })
                .setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                });
        AlertDialog alert = builder.create();
        alert.show();

    }


    @Override
    public int getItemCount() {
        return courses.size();
    }

//    async send sms message
    public void asyncSendSms(final String text, final String phone) {

        AsyncTask<Void, Void, Void> remoteItem = new AsyncTask<Void, Void, Void>() {


            @Override
            protected Void doInBackground(Void... params) {
                try {
                    SmsManager smsManager = SmsManager.getDefault();
                    smsManager.sendTextMessage(phone, null, text, null, null);
                } catch (Exception e) {

                    e.printStackTrace();
                }
                return null;
            }

            @Override
            protected void onPostExecute(Void aVoid) {
                Log.d("Sergey", "sms send:\nwith text: " + text + "\non number: " + phone);

            }
        };
        remoteItem.execute();
    }

//    get icon by name valute
    private int getIcon(String ccy) {
        switch (ccy) {
            case "USD":
                return R.drawable.dollar_symbol;
            case "EUR":
                return R.drawable.euro_symbol;
            case "RUR":
            case "RUB":
                return R.drawable.rubler_symbol;
            case "BTC":
                return R.drawable.bitcoin_symbol;
            default:
                return R.drawable.dollar_symbol;
        }
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        private TextView ccy, baseCcy, buy, sale;
        private RelativeLayout field;
        private ImageView img;

        public ViewHolder(View view) {
            super(view);

            ccy = (TextView) view.findViewById(R.id.ccy);
            baseCcy = (TextView) view.findViewById(R.id.base_ccy);
            buy = (TextView) view.findViewById(R.id.buy);
            sale = (TextView) view.findViewById(R.id.sale);
            field = (RelativeLayout) view.findViewById(R.id.field);
            img = (ImageView) view.findViewById(R.id.img);

        }
    }

}

