package sergey.zhuravel.currencyrate.adapter;

import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import io.realm.RealmViewHolder;
import sergey.zhuravel.currencyrate.R;


public class ViewHolderValuteRVH extends RealmViewHolder {
    public TextView date;
    public TextView buy;
    public TextView sale;
    public LinearLayout field;

    public ViewHolderValuteRVH(View itemView) {
        super(itemView);
        date = (TextView) itemView.findViewById(R.id.date);
        buy = (TextView) itemView.findViewById(R.id.buy);
        sale = (TextView) itemView.findViewById(R.id.sale);
        field = (LinearLayout) itemView.findViewById(R.id.field);
    }
}
