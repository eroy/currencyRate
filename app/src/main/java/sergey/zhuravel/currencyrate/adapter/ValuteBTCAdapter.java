package sergey.zhuravel.currencyrate.adapter;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import io.realm.Realm;
import io.realm.RealmBasedRecyclerViewAdapter;
import io.realm.RealmResults;
import sergey.zhuravel.currencyrate.R;
import sergey.zhuravel.currencyrate.fragment.MainFragment;
import sergey.zhuravel.currencyrate.model.ValuteBTC;
import sergey.zhuravel.currencyrate.model.ValuteRUB;
import sergey.zhuravel.currencyrate.utils.RealmController;



public class ValuteBTCAdapter extends RealmBasedRecyclerViewAdapter<ValuteBTC, ViewHolderValuteRVH> {
    private Realm realm;
    private Context context;

    public ValuteBTCAdapter(Context context, RealmResults<ValuteBTC> realmResults, boolean automaticUpdate, boolean animateResults) {
        super(context, realmResults, automaticUpdate, animateResults);
        this.context = context;
    }

    @Override
    public ViewHolderValuteRVH onCreateRealmViewHolder(ViewGroup viewGroup, int i) {
        View view = inflater.inflate(R.layout.card_details, viewGroup, false);
        return new ViewHolderValuteRVH((LinearLayout) view);
    }

    @Override
    public void onBindRealmViewHolder(ViewHolderValuteRVH viewHolderRecyclerView, int i) {
        realm = RealmController.getInstance().getRealm();

        final ValuteBTC valuteBTC = realmResults.get(i);

        viewHolderRecyclerView.date.setText(MainFragment.formatStringToDateFull(valuteBTC.getDate()));
        viewHolderRecyclerView.buy.setText(valuteBTC.getBuy());
        viewHolderRecyclerView.sale.setText(valuteBTC.getSale());


        viewHolderRecyclerView.field.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
//                etc..
            }
        });


    }


    @Override
    public int getItemCount() {
        return realmResults.size();
    }

}