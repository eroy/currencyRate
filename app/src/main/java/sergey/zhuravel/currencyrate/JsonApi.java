package sergey.zhuravel.currencyrate;

import com.google.gson.JsonElement;

import retrofit2.Call;
import retrofit2.http.GET;

public interface JsonApi {

    @GET("/p24api/pubinfo?exchange&json&coursid=11")
    Call<JsonElement> getCourses();

}
